# frozen_string_literal: true

describe Gitlab::QA::Report::RelateFailureIssue do
  describe '#invoke!' do
    let(:project) { 'valid-project' }
    let(:test_name) { 'Manage Users API GET /users' }
    let(:test_file_partial) { 'api/1_manage/users_spec.rb' }
    let(:test_file_full) { "./qa/specs/features/#{test_file_partial}" }
    let(:testcase_url) { 'https://gitlab.com/gitlab-org/quality/testcases/-/issues/460' }
    let(:ci_job_url) { 'https://gitlab.com/gitlab-org/gitlab-qa/-/jobs/806591854' }
    let(:failure_lines) do
      [
        'Failure/Error: expect_status(404)',
        '',
        '  expected: 404',
        '       got: 200',
        '',
        '  (compared using ==)'
      ]
    end

    let(:test_data) do
      <<~JSON
        {
          "examples": [
            {
              "id":"#{test_file_full}[1:1:1]",
              "description":"GET /users",
              "full_description": "#{test_name}",
              "status":"failed",
              "file_path":"#{test_file_full}",
              "line_number":11,
              "run_time":0.31676485,
              "pending_message":null,
              "testcase":"#{testcase_url}",
              "quarantine":null,
              "screenshot":{},
              "ci_job_url":"#{ci_job_url}",
              "exceptions":[
                {
                  "class":"RSpec::Expectations::ExpectationNotMetError",
                  "message":"\\nexpected: 404\\n     got: 200\\n\\n(compared using ==)\\n",
                  "message_lines":#{failure_lines.to_json},
                  "backtrace":[
                    "/usr/local/bundle/gems/airborne-0.3.4/lib/airborne/request_expectations.rb:36:in `expect_status'",
                    "./qa/specs/features/api/1_manage/users_spec.rb:14:in `block (3 levels) in <module:QA>'"
                  ]
                }
              ]
            }
          ]
        }
      JSON
    end

    let(:issue_class) { Struct.new(:iid, :web_url, :state, :title, :description, :labels) }
    let(:new_issue) { issue_class.new(0, 'http://new-issue.url') }
    let(:expected_posted_note) { "/relate #{testcase_url}" }
    let(:expected_new_issue_description) do
      "### Full description\n\n#{test_name}\n\n### File path\n\n#{test_file_full}\n\n" \
      "### Stack trace\n\n```\n#{failure_lines.join("\n")}\n```\n\nFirst happened in #{ci_job_url}."
    end

    around do |example|
      ClimateControl.modify(CI_PROJECT_NAME: 'staging') { example.run }
    end

    describe 'NEW_ISSUE_LABELS' do
      it { expect(described_class::NEW_ISSUE_LABELS).to eq(Set.new(%w[QA Quality test failure::investigating priority::2])) }
    end

    context 'with valid input' do
      subject { described_class.new(token: 'token', input_files: 'files', project: project) }

      before do
        allow(subject).to receive(:assert_input_files!)
        allow(subject.__send__(:gitlab)).to receive(:assert_user_permission!)
        allow(::Dir).to receive(:glob).and_return([test_file])
        allow(::File).to receive(:read).with(test_file).and_return(test_data)
      end

      context 'when an issue exists for a given test' do
        context 'when the results are in JSON format' do
          let(:test_file) { 'file.json' }
          let(:issues) { [] }
          let(:issue_description) do
            <<~TEXT
### Full description

Manage Users API GET /users

### File path

./qa/specs/features/api/1_manage/users_spec.rb

### Stack trace

```
Failure/Error: expect_status(404)

  expected: 404
       got: 200

  (compared using ==)
```
            TEXT
          end

          before do
            allow(::File).to receive(:write)
            allow(::Gitlab).to receive(:issues)
              .with(anything, { state: 'opened', labels: 'QA' })
              .and_return(Struct.new(:auto_paginate).new(issues))
          end

          context 'when no failures are present' do
            let(:test_data) do
              <<~JSON
                {
                  "examples": [
                    {
                      "full_description": "#{test_name}",
                      "file_path": "#{test_file_full}"
                    }
                  ]
                }
              JSON
            end

            it 'does not find the issue' do
              expect { subject.invoke! }.not_to output(/Found issue/).to_stdout
            end
          end

          context 'when no issue is found' do
            it 'creates a new issue' do
              expect(subject).to receive(:create_issue).and_return(new_issue)
              expect(subject).to receive(:update_labels)
              expect(subject).to receive(:post_failed_job_note)

              expect { subject.invoke! }
                .to output(%r{Created new issue: http://new-issue.url for test '#{test_name}'.*}).to_stdout
            end

            context 'when creating a new issue' do
              it 'creates the issue in the provided project and post the failed job url' do
                expect(::Gitlab).to receive(:create_issue)
                  .with(
                    project,
                    "Failure in #{test_file_partial} | #{test_name}",
                    hash_including(description: expected_new_issue_description, labels: described_class::NEW_ISSUE_LABELS.to_a + ['found:staging.gitlab.com']))
                  .and_return(new_issue)
                expect(::Gitlab).to receive(:edit_issue).with(anything, anything, labels: %w[found:staging.gitlab.com])
                expect(::Gitlab).to receive(:create_issue_note).with(project, new_issue.iid, expected_posted_note)

                expect { subject.invoke! }.to output.to_stdout
              end

              context 'when the issue title would be longer than the maximum allowed' do
                let(:test_file_partial) { 'api/3_create/gitaly/changing_repository_storage_spec.rb' }
                let(:test_name) do
                  'Create Changing Gitaly repository storage when moving from Gitaly to Gitaly Cluster behaves like ' \
                  'repository storage move confirms a `finished` status after moving project repository storage'
                end

                it 'creates the issue in the provided project and post the failed job url' do
                  full_title = "Failure in #{test_file_partial} | #{test_name}"

                  expect(::Gitlab).to receive(:create_issue)
                    .with(anything, "#{full_title[0...described_class::MAX_TITLE_LENGTH - 3]}...", anything)

                  expect { subject.invoke! }.to output.to_stdout
                end
              end
            end
          end

          context 'when one issue is found' do
            let(:issue) do
              issue_class.new(1, 'http://existing-issue.url/issue1', 'opened', issue_title, issue_description)
            end

            let(:issues) { [issue] }

            before do
              allow(subject).to receive(:update_labels)
              allow(subject).to receive(:post_failed_job_note)
            end

            context 'when test name matches issue title' do
              let(:issue_title) { "Hello #{test_name} world!" }

              it 'finds the issue' do
                expect { subject.invoke! }.to output(/Found issue #{issue.web_url} for test '#{test_name}' with a diff ratio of 0.0%./).to_stdout
              end

              it 'posts the failed job url' do
                expect(subject).to receive(:update_labels).and_call_original
                expect(::Gitlab).to receive(:edit_issue).with(anything, anything, labels: ['found:staging.gitlab.com'])
                expect(subject).to receive(:post_failed_job_note).and_call_original
                expect(::Gitlab).to receive(:create_issue_note).with(project, issue.iid, expected_posted_note)

                expect { subject.invoke! }.to output.to_stdout
              end
            end

            context 'when test path matches issue title' do
              let(:issue_title) { "Hello #{test_file_partial} world!" }

              it 'finds the issue' do
                expect { subject.invoke! }.to output(/Found issue #{issue.web_url} for test '#{test_name}' with a diff ratio of 0.0%./).to_stdout
              end
            end

            context 'when test matches issue but stacktrace is too different' do
              let(:issues) do
                [issue_class.new(1, 'http://existing-issue.url/issue1', 'opened', test_name, issue_description.gsub('404', '513'))]
              end

              it 'creates the issue in the provided project' do
                expect(::Gitlab).to receive(:create_issue)
                  .with(
                    project,
                    "Failure in #{test_file_partial} | #{test_name}",
                    hash_including(description: expected_new_issue_description, labels: described_class::NEW_ISSUE_LABELS.to_a + ['found:staging.gitlab.com']))
                  .and_return(new_issue)

                expect { subject.invoke! }.to output.to_stdout
              end
            end
          end

          context 'when multiple issues are found' do
            let(:issue1) do
              issue_class.new(1, 'http://existing-issue.url/issue1', 'opened', test_name, issue_description)
            end

            let(:issue2) do
              issue_class.new(2, 'http://existing-issue.url/issue2', 'opened', test_name, issue_description)
            end

            let(:issues) { [issue1, issue2] }

            before do
              allow(subject).to receive(:update_labels)
              allow(subject).to receive(:post_failed_job_note)
            end

            it 'displays a warning when multiple issues are found and none is a better match than the other' do
              expect { subject.invoke! }.to output(/Too many issues found for test '#{test_name}' \(`#{test_file_full}`\)!/).to_stderr
              expect { subject.invoke! }.not_to output(/Found issue/).to_stdout
              expect { subject.invoke! }.not_to output(/Created new issue/).to_stdout
            end

            context 'when one issue is a better match than the other' do
              let(:issue2) do
                issue_class.new(2, 'http://existing-issue.url/issue2', 'opened', test_name, issue_description.gsub('404', '503'))
              end

              it 'returns the best matching issue' do
                expect { subject.invoke! }.to output(/Found issue #{issue1.web_url} for test '#{test_name}' with a diff ratio of 0.0%./).to_stdout
              end

              it 'posts the failed job url' do
                expect(subject).to receive(:update_labels).and_call_original
                expect(::Gitlab).to receive(:edit_issue).with(anything, anything, labels: ['found:staging.gitlab.com'])
                allow(subject).to receive(:post_failed_job_note).and_call_original
                expect(::Gitlab).to receive(:create_issue_note).with(project, issue1.iid, expected_posted_note)

                expect { subject.invoke! }.to output.to_stdout
              end
            end

            context 'when the issue has an empty stacktrace' do
              let(:issue2) do
                issue_class.new(2, 'http://existing-issue.url/issue2', 'opened', test_name, issue_description.gsub(/```.+```/m, "```\n\n```"))
              end

              let(:test_data) do
                <<~JSON
                  {
                    "examples": [
                      {
                        "full_description": "#{test_name}",
                        "file_path": "#{test_file_full}",
                        "exceptions":[
                          {
                            "class": "Capybara::ElementNotFound",
                            "message": "Unable to find css \\"[data-qa-selector='new_user_first_name_field'],.qa-new-user-first-name-field\\"",
                            "message_lines": [          ],
                            "backtrace": [          ]
                          }
                        ]
                      }
                    ]
                  }
                JSON
              end

              it 'finds the issue' do
                expect { subject.invoke! }.to output(/Found issue #{issue2.web_url} for test '#{test_name}' with a diff ratio of 0.0%./).to_stdout
              end
            end
          end
        end
      end
    end
  end
end
