# frozen_string_literal: true

describe Gitlab::QA::Report::GenerateTestSession do
  let(:test_data_path) { 'test_data_path' }
  let(:test_data) do
    <<~JSON
      {
        "examples": [
          {
            "full_description": "test-name-01",
            "file_path": "#{file_path_base}/test_01_spec.rb",
            "testcase": "#{testcase}/1",
            "ci_job_url": "/10",
            "status": "passed",
            "quarantine": null
          },
          {
            "full_description": "test-name-02",
            "file_path": "#{file_path_base}/test_02_spec.rb",
            "testcase": "#{testcase}/2",
            "ci_job_url": "/20",
            "status": "pending"
          },
          {
            "full_description": "test-name-03",
            "file_path": "#{file_path_base}/test_03_spec.rb",
            "testcase": "#{testcase}/3",
            "ci_job_url": "/30",
            "status": "failed"
          },
          {
            "full_description": "test-name-04",
            "file_path": "#{file_path_base}/test_04_spec.rb",
            "testcase": "#{testcase}/4",
            "ci_job_url": "/40",
            "status": "unknown"
          },
          {
            "full_description": "test-name-05",
            "file_path": "qa/specs/features/browser_ui/plan/test_05_spec.rb",
            "testcase": "#{testcase}/5",
            "ci_job_url": "/50",
            "status": "passed",
            "quarantine": "https://gitlab.com/gitlab-org/quality/testcases/-/issues/12345"
          },
          {
            "full_description": "test-name-06",
            "file_path": "#{file_path_base}/test_01_spec.rb",
            "testcase": "#{testcase}/1",
            "ci_job_url": "/60",
            "status": "passed"
          },
          {
            "full_description": "test-name-03",
            "file_path": "#{file_path_base}/test_03_spec.rb",
            "testcase": "#{testcase}/3",
            "ci_job_url": "/70",
            "status": "failed",
            "quarantine": { "issue": "https://gitlab.com/gitlab-org/quality/testcases/-/issues/12345" }
          },
          {
            "full_description": "test-name-02",
            "file_path": "#{file_path_base}/test_02_spec.rb",
            "testcase": "#{testcase}/2",
            "ci_job_url": "/80",
            "status": "crashed"
          },
          {
            "full_description": "test-name-07",
            "file_path": "#{file_path_base}/test_07_spec.rb",
            "testcase": "#{testcase}/7",
            "ci_job_url": "/90",
            "status": "failed",
            "failure_issue": "/failures/90"
          },
          {
            "full_description": "test-name-07",
            "file_path": "#{file_path_base}/test_07_spec.rb",
            "testcase": "#{testcase}/7",
            "ci_job_url": "/90",
            "status": "failed",
            "failure_issue": "/failures/90"
          }
        ]
      }
    JSON
  end

  let(:file_path_base) { 'qa/specs/features/browser_ui/stage' }
  let(:testcase) { 'https://gitlab.com/gitlab-org/quality/testcases/-/issues' }

  subject { described_class.new(token: 'token', project: 'project', input_files: [double]) }

  before do
    allow(subject).to receive(:validate_input!)
    allow(Dir).to receive(:glob).and_return([test_data_path])
    allow(File).to receive(:read).and_return(test_data)

    allow(Gitlab::QA::Runtime::Env).to receive(:pipeline_from_project_name).and_return('staging')
    allow(Gitlab::QA::Runtime::Env).to receive(:deploy_version).and_return('deadbeef')
    allow(Gitlab::QA::Runtime::Env).to receive(:deploy_environment).and_return('gstg')
    allow(Gitlab::QA::Runtime::Env).to receive(:ci_pipeline_url).and_return('ci_pipeline_url')
    allow(Gitlab::QA::Runtime::Env).to receive(:ci_pipeline_id).and_return('ci_pipeline_id')
    allow(Gitlab::QA::Runtime::Env).to receive(:ci_project_id).and_return('ci_project_id')
    allow(Gitlab::QA::Runtime::Env).to receive(:qa_issue_url).and_return('https://example.com/qa/issue')
    allow(Gitlab::QA::Runtime::Env).to receive(:ci_api_v4_url).and_return('https://gitlab/api/v4')
    allow(Gitlab::QA::Runtime::Env).to receive(:gitlab_ci_api_token).and_return('ci_api_token')

    stub_request(:get, 'https://gitlab/api/v4/projects/ci_project_id/pipelines/ci_pipeline_id/jobs?scope=failed')
      .with(headers:
      {
        'Private-Token' => 'ci_api_token'
      })
      .to_return(body:
      [
        { allow_failure: true, name: 'A', web_url: 'AURL' },
        { allow_failure: false, name: 'B', web_url: 'BURL' }
      ].to_json)
  end

  describe '#invoke!' do
    it 'creates an issue as test session report' do
      description = <<~MARKDOWN.chomp
      ## Session summary

      * Deploy version: deadbeef
      * Deploy environment: gstg
      * Pipeline: staging [ci_pipeline_id](ci_pipeline_url)
      * Total 10 tests
      * Passed 3 tests
      * Failed 4 tests
      * 3 other tests (usually skipped)

      ## Failed jobs

      * [A](AURL) (allowed to fail)
      * [B](BURL)

      ### Plan

      * Total 1 tests
      * Passed 1 tests
      * Failed 0 tests
      * 0 other tests (usually skipped)

      <details><summary>Passed tests:</summary>

      | Test | Job | Status | Action |
      | - | - | - | - |
      [test-name-05](https://gitlab.com/gitlab-org/quality/testcases/-/issues/5) | [50](/50) ~"quarantine" | ~"passed" | -

      </details>

      ### Stage

      * Total 9 tests
      * Passed 2 tests
      * Failed 4 tests
      * 3 other tests (usually skipped)

      | Test | Job | Status | Action |
      | - | - | - | - |
      [test-name-03](https://gitlab.com/gitlab-org/quality/testcases/-/issues/3) | [30](/30), [70](/70) ~"quarantine" | ~"failed" | <ul><li>[ ] failure issue exists or was created</li></ul>
      [test-name-07](https://gitlab.com/gitlab-org/quality/testcases/-/issues/7) | [90](/90) | ~"failed" | <ul><li>[ ] [failure issue](/failures/90)</li></ul>

      <details><summary>Passed tests:</summary>

      | Test | Job | Status | Action |
      | - | - | - | - |
      [test-name-01, test-name-06](https://gitlab.com/gitlab-org/quality/testcases/-/issues/1) | [10](/10), [60](/60) | ~"passed" | -

      </details>

      <details><summary>Other tests:</summary>

      | Test | Job | Status | Action |
      | - | - | - | - |
      [test-name-02](https://gitlab.com/gitlab-org/quality/testcases/-/issues/2) | [20](/20), [80](/80) | ~"pending", ~"crashed" | -
      [test-name-04](https://gitlab.com/gitlab-org/quality/testcases/-/issues/4) | [40](/40) | ~"unknown" | -

      </details>

      ## Release QA issue

      * https://example.com/qa/issue

      /relate https://example.com/qa/issue
      MARKDOWN

      stub_request(:post, 'https://gitlab.com/api/v4/projects/project/issues')
        .with(body:
        {
          'title' => 'Test session report | staging',
          'description' => description,
          'labels' => ['Quality', 'QA', 'triage report', 'found:staging.gitlab.com']
        })
        .to_return(body: { 'web_url' => 'issue_url' }.to_json)

      expect(File).to receive(:write).with('REPORT_ISSUE_URL', 'issue_url')

      expect { subject.invoke! }.to output.to_stdout
    end
  end
end
