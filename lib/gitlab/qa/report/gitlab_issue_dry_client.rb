# frozen_string_literal: true

module Gitlab
  module QA
    module Report
      class GitlabIssueDryClient < GitlabIssueClient
        def create_issue(title:, description:, labels:)
          attrs = { description: description, labels: labels }

          puts "The following issue would have been created:"
          puts "project: #{project}, title: #{title}, attrs: #{attrs}"
        end

        def edit_issue(iid:, options: {})
          puts "The #{project}##{iid} issue would have been updated with: #{options}"
        end

        def create_issue_note(iid:, note:)
          puts "The following note would have been posted on #{project}##{iid} issue: #{note}"
        end

        def add_note_to_issue_discussion_as_thread(iid:, discussion_id:, body:)
          puts "The following discussion note would have been posted on #{project}##{iid} (discussion #{discussion_id}) issue: #{body}"
        end
      end
    end
  end
end
